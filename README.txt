About
=====
This module was built upon the idea that since Drupal 7 modules can be
installed through the Modules page without the need to ever go to the
command line. Many modules include a README file that contain important
information about installation and configuration. Generally, you would need to
get to a commandline or an external website to read these.

This module aims to fix this by searching for the readme file and linking to
it on the module page.
